import java.util.Scanner;

public class Menu {
    public Menu() {
    }

    private void MakeDiv(int n) {
        for (int i = 0; i <= n; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    private void clearCL() {
        System.out.print("\033[H\033[2J");
    }

    protected void run() {
        clearCL();
        Scanner scan = new Scanner(System.in);
        boolean exit = false;
        String option = "";

        while (!exit) {
            MakeDiv(12);
            System.out.println("Choose one: ");
            System.out.println("1. Option A.");
            System.out.println("2. Option B.");
            System.out.println("3. Option C.");
            System.out.println("e. Exit.");
            MakeDiv(12);

            option = scan.nextLine();
            clearCL();

            switch (option) {
                case "1":
                    System.out.println("Option 1 selected.");
                    break;
                case "2":
                    System.out.println("Option 2 selected.");
                    break;
                case "3":
                    System.out.println("Option 3 selected.");
                    break;
                case "e":
                    System.out.print("Are you sure? (Y/n): ");
                    option = scan.nextLine();
                    clearCL();
                    switch (option) {
                        case "":
                        case "Y":
                        case "y":
                            exit = true;
                            break;
                        default:
                            exit = false;
                            break;
                    }
                    break;
                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }
    }
}